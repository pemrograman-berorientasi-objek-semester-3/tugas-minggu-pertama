public class main {
    public static void main(String[] args) {
        int angka=7;

        for(int i=0; i<angka; i++){
            if(i%2==0){
                System.out.println(i+" adalah angka genap");
            }else{
                System.out.println(i+" adalah angka ganjil");
            }
        }

        int j=0;
        while(j<angka){
            System.out.println("perulangan ke"+j);
            j++;
        }

        int k=0;
        do{
            System.out.println("perulangan ke"+k);
            k++;
        }while(k<angka);

        int[] arraySatuDimensi={21, 23, 20, 22, 24, 25};
        int[][] arrayMultiDimensi={{11, 12, 13}, {14, 15, 16}};

        java.util.Scanner scanner = new java.util.Scanner(System.in);
        System.out.print("Masukkan nilai : ");
        int inputNilai = scanner.nextInt();
        System.out.println("Nilai yang dimasukkan adalah : " +inputNilai);

    }
}
