#include <iostream>
using namespace std;

int main(){
    int angka=7;

    for(int i=0; i<angka; i++) {
        if(i%2==0) {
            cout<<i<<" adalah angka genap"<<endl;
        }else{
            cout<<i<<" adalah angka ganjil"<<endl;
        }
    }

    int j=0;
    while(j<angka) {
        cout<<"perulangan ke"<<j<<endl;
        j++;
    }

    int k=0;
    do{
        cout<<"perulangan ke"<<k<<endl;
        k++;
    }while(k<angka);

    int arraySatuDimensi[6] = {21, 23, 20, 22, 24, 25};
    int arrayMultiDimensi[2][3] = {{11, 12, 13}, {14, 15, 16}};

    int inputNilai;
    cout << "Masukkan nilai : ";cin>>inputNilai;
    cout<<"Nilai yang dimasukkan adalah : "<<inputNilai<<endl;
}
